const warning = "Invalid author field! Forename or surname must start from letter \"A\"";

function isValid(author) {
    const name = author.toString().split(" ");
    const forename = name[0];
    const surname = name[name.length - 1];
    return forename.charAt(0) == "A" || surname.charAt(0) == "A";
}

function showWarning() {
    $('#warning_msg').text(warning);
    $('#author').css("borderColor", "red");
}