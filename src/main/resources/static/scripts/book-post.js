$(document).ready(function () {
    $('#add-book-form').submit(function (event) {
            event.preventDefault();

            if (!isValid($('#author').val())) {
                showWarning();
            } else {
                const newBook = {
                    "author": $('#author').val(),
                    "title": $('#title').val(),
                    "isbn": $('#isbn').val()
                };
                $.ajax({
                    type: "POST",
                    url: "books",
                    contentType: "application/json",
                    data: JSON.stringify(newBook),
                    success: function () {
                        window.location.replace("/library.html");
                    },
                    error: function () {
                        showWarning();
                    }
                })
            }
        }
    )
})