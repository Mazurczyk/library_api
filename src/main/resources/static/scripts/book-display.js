$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "books",
        dataType: "json",
        success: function (bookList) {
            if ($.trim(bookList)) {
                $.each(bookList, function (i, book) {
                    $('<li/>').text("Author: " + book.author +
                        "; Title: " + book.title +
                        "; ISBN: " + book.isbn +
                        ";")
                        .appendTo($('#book_list'));
                });
            } else {
                $('<p/>').text("No records").appendTo($('#book_list'));
            }
        }
    })
});