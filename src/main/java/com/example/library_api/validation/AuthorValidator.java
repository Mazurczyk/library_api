package com.example.library_api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.Stream;

public class AuthorValidator implements ConstraintValidator<AuthorConstraint, String> {

    private final static String VALID_LETTER = "A";

    public void initialize(AuthorConstraint constraint) {
    }

    public boolean isValid(String author, ConstraintValidatorContext context) {
        return Stream.of(author)
                .map(a -> a.split(" "))
                .anyMatch(name -> {
                    String forename = name[0];
                    String surname = name[name.length - 1];
                    return forename.startsWith(VALID_LETTER) || surname.startsWith(VALID_LETTER);
                });
    }
}
