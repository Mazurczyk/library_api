package com.example.library_api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AuthorValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorConstraint {
    String message() default "Invalid author field! Forename or surname must start from letter A";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}