package com.example.library_api.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class BookService {

    private final BookRepository bookRepository;
    private final Logger logger;

    @Autowired
    public BookService(BookRepository bookRepository, Logger logger) {
        this.bookRepository = bookRepository;
        this.logger = logger;
    }

    public Book addBook(Book book) {
        logger.info("New record has been added to database");
        return bookRepository.save(book);
    }

    public List<Book> getAllBooks() {
        logger.info("The records has been retrieved from database");
        return bookRepository.findAll();
    }
}
