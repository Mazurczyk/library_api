package com.example.library_api.book;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class BookRepositoryTest {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    TestEntityManager testEntityManager;

    Book book = new Book("Agatha Christie",
            "Murder on the Orient Express",
            "9780008249670");

    @Test
    void when_no_data_then_return_empty_list() {
        //given

        //when
        List<Book> books = bookRepository.findAll();
        //then
        assertNotNull(books);
        assertTrue(books.isEmpty());
    }

    @Test
    void given_item_when_is_added_to_db_then_reflect_book_list_size() {
        //given

        //when
        testEntityManager.persist(book);
        List<Book> books = bookRepository.findAll();
        //then
        assertNotNull(books);
        assertEquals(1, books.size());
        assertEquals(Arrays.asList(book), books);
    }

    @Test
    void save_item_then_return_proper_one() {
        //given

        //when
        bookRepository.save(book);
        Book addedBook = bookRepository.findOne(Example.of(book)).get();
        //then
        assertEquals(book, addedBook);
    }
}