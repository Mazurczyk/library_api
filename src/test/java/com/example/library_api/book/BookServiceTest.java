package com.example.library_api.book;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.logging.Logger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class BookServiceTest {

    @TestConfiguration
    static class BookServiceConfig {
        @Bean
        public BookService bookService(BookRepository bookRepository, Logger logger) {
            return new BookService(bookRepository, logger);
        }
    }

    @Autowired
    private BookService bookService;

    @MockBean
    private BookRepository bookRepository;

    @MockBean
    private Logger logger;

    private Book book = new Book("Agatha Christie",
            "Murder on the Orient Express",
            "9780008249670");

    @Test
    void given_item_when_is_added_then_save_repository_method_should_be_executed() {
        //given

        //when
        bookService.addBook(book);
        //then
        verify(bookRepository).save(book);
    }

    @Test
    void when_all_books_are_got_then_findAll_repository_method_should_be_executed() {
        //given

        //when
        bookService.getAllBooks();
        //then
        verify(bookRepository).findAll();
    }
}