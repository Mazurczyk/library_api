package com.example.library_api.book;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BookController.class)
class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    private Book book = new Book("Agatha Christie",
            "Murder on the Orient Express",
            "9780008249670");

    private String jsonValidBook = "{" +
            "\"author\":\"Agatha Christie\"," +
            "\"title\":\"Murder on the Orient Express\"," +
            "\"isbn\":\"9780008249670\"" +
            "}";

    private String jsonInvalidBook = "{" +
            "\"author\":\"James Rollins\"," +
            "\"title\":\"Amazonia\"," +
            "\"isbn\":\"9783641096205\"" +
            "}";

    @Test
    void return_book_list_when_request_whole_table() throws Exception {
        //given
        when(bookService.getAllBooks()).thenReturn(Collections.singletonList(book));
        //when and then
        mockMvc.perform(get("/books")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    void when_valid_author_then_insert_record() throws Exception {
        //given

        //when and then
        mockMvc.perform(post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonValidBook))
                .andExpect(status().isOk());
    }

    @Test
    void when_invalid_author_then_book_should_not_be_added() throws Exception {
        //given

        //when and then
        mockMvc.perform(post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonInvalidBook))
                .andExpect(status().is4xxClientError());
    }
}